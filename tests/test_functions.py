#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

from dotenv import dotenv_values

from iut_pe.scripts import fetch, build
from iut_pe.helpers import FileHandler


def test_fetch():

    env = dotenv_values(".env")
    config = {
        "scodoc": {
            "url": env["SCODOC_URL"],
            "login": env["SCODOC_LOGIN"],
            "password": env["SCODOC_PASSWORD"],
            "departement": env["SCODOC_DEPARTEMENT"],
        },
        "paths": {},
    }
    fetch(config, reset=True, EID=503)
    fetch(config, EID=503, lazy=False)
    fetch(config, EID=503)
    fetch(config, EID=1)


def test_build():
    config = {
        "paths": {
            "database": "./tests/static/michael_palin.json",
            "logo": "./tests/static/logo.png",
            "sign": "./tests/static/sign.png",
            "latex": "./tests/output/latex",
            "pdf": "./tests/output/pdf",
        },
        "latex": {
            "name": "Mister Mister",
            "city": "Ma ville",
            "address": [
                "IUT 1 de Ma ville",
                "Département Génie Civil - Construction Durable",
                "13100 Ma ville",
                "moi@monuniv.fr",
            ],
        },
    }
    etudiants = FileHandler(config["paths"]["database"]).load()
    build(etudiants, config)
    config["comments"] = {"apo": {"TBB1FI": "Note TBB1FI"}, "all": "Note all"}
    build(etudiants, config)
