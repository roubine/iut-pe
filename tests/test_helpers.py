#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

from dotenv import dotenv_values

from iut_pe.helpers import FileHandler, ScodocAPI


def test_scodoc():

    env = dotenv_values(".env")
    scodoc = ScodocAPI(
        url=env["SCODOC_URL"],
        login=env["SCODOC_LOGIN"],
        password=env["SCODOC_PASSWORD"],
        departement=env["SCODOC_DEPARTEMENT"],
    )

    print(scodoc)
    scodoc.call("etudiants/courants")
    scodoc.ping()


def test_filehandler():

    f = FileHandler("dummy.json")
    print(f)
    if f:
        print("f is true")
    f.create_path()
    if f:
        print("f is True")

    d = FileHandler("dummy", is_file=False)
    print(d)
    d.create_path()
    # d.create_path()
    if d:
        print("d is True")
    d.delete_file()

    f = FileHandler(["dummy2", "my", "dummy.json"])
    f.create_path()

    data = {"hello": "world"}
    f.dump(data)

    data_read = f.load()
    assert data_read["hello"] == "world"

    f.delete_file()

    config = FileHandler("yolo")
    try:
        config.load(config=True)
    except FileNotFoundError:
        pass
    config = FileHandler(["tests", "static", "config1.yml"])
    config.load(config=True)
    config = FileHandler(["tests", "static", "config2.yml"])
    c = config.load(config=True)
    assert "paths" in c
