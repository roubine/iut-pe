#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

from loop_rate_limiters import RateLimiter
from iut_pe.helpers import ScodocAPI, Student, FileHandler, default_parser
import os
import subprocess
import json

def ping():
    """Fiche poursuite d'études ScoDoc
    iut-pe-ping: permet de tester la connection avec l'API de ScoDoc.
    """
    # args parser
    parser = default_parser("iut-pe-ping", ping.__doc__)
    args = parser.parse_args()
    config = FileHandler(args.config).load(config=True)
    api = ScodocAPI(**config["scodoc"])
    api.ping()


def fetch(config, lazy=True, reset=False, EID=None, SID=None):
    #######################
    # Initialise database #
    #######################
    database = FileHandler(config["paths"].get("database", "./etudiants.json"))
    if reset:
        database.delete_file()
    try:
        students_db = database.load()
    except Exception:
        print(f"Initialisation du fichier {database}")
        students_db = {}
        database.dump(students_db)
    print(f"Base de données: {database}")

    ####################################
    # get list of students from ScoDoc #
    ####################################
    api = ScodocAPI(**config["scodoc"])
    if EID:
        print(f"Récupération des données pour l'étudiant {EID}", end="... ")
        students = api.call(f"etudiants/etudid/{EID}")
    elif SID:
        print(f"Récupération des données pour les étudiants du semestre {SID}", end="... ")
        students = api.call(f"formsemestre/{SID}/etudiants")
    else:
        print("Récupération des données pour les étudiants de tous les semestres courants", end="... ")
        students = api.call("etudiants/courants")
    print(f" -> {len(students)} students found.")

    ##############################################
    # helper function to get semesters decisions #
    ##############################################
    def get_jury(semester_id, student_id, jury):

        # get jury for all students if not already fetch in the API
        if semester_id not in jury:
            print(f"\t... looking for semester {semester_id}")
            jury[semester_id] = api.call(f'formsemestre/{semester_id}/decisions_jury')
            with open('jury_debug.json', 'w') as f:
                json.dump(jury, f, indent=4)

        for student in jury[semester_id]:
            if student_id == student["etudid"]:
                return student["rcues"]
        
        return None

    try:
        with open('jury_debug.json') as f:
            jury = json.load(f)
            jury = {int(k): v for k, v in jury.items()}
    except:
        jury = dict()
    
    ###################################################
    # loop over the students to build up the database #
    ###################################################    
    for i, student in enumerate(students):
        disp = f'{i + 1:03d}/{len(students):03d} {student["civilite"]} {student["nom"]} {student["prenom"].title()} [{student["id"]}]'
        print(f"{disp:<40}")

        if lazy and str(student["id"]) in students_db:
            # print("ok")
            continue

        for k, v in api.call(f'etudiants/etudid/{student["id"]}')[0].items():
            student[k] = v
        
        try:
            semesters = api.call(f'etudiant/etudid/{student["id"]}/formsemestres')
        except Exception as e:
            print(f"error (étudiant non trouvé: {e})")
            continue

        # print()
        student["semestres"] = []
        for si, semester in enumerate(semesters):
            print(f'\t{semester["titre_num"]} {semester["date_debut"]} -> {semester["date_fin"]} [{semester["formsemestre_id"]}]')
            marks = api.call(f'etudiant/etudid/{student["id"]}/formsemestre/{semester["id"]}/bulletin')
            semester["resultats"] = {k: marks[k] for k in ["ues", "ues_capitalisees"]}
            semester["groupes"] = marks["semestre"]["groupes"]
            semester["absences"] = marks["semestre"]["absences"]
            semester["rang"] = marks["semestre"]["rang"]
            semester["competences"] = get_jury(semester["id"], student["id"], jury)
            semester["i"] = si
            student["semestres"].append(semester)
        students_db[str(student["id"])] = student
        database.dump(students_db)


def fetch_script():
    """Récupère les informations étudiants depuis ScoDoc et les enregistre dans une base de donnée locale."""
    # args parser
    parser = default_parser("iut-pe-fetch", fetch.__doc__)
    parser.add_argument("--lazy", action="store_true", help="Ignore les étudiants déjà dans la base de données.")
    parser.add_argument("--reset", action="store_true", help="Reconstruit la base de données à partir de zéro.")
    parser.add_argument("--etudid", dest="EID", default=None, type=int, help="Enter l'identifiant ScoDoc d'un étudiant pour uniquement récupérer les informations de cet étudiant.")
    parser.add_argument("--semestreid", dest="SID", default=None, type=int, help="Enter l'identifiant ScoDoc d'un semestre pour uniquement récupérer les informations des étudiants de ce semestre.")
    args = parser.parse_args()

    ######################
    # configuration file #
    ######################
    config = FileHandler(args.config).load(config=True)

    fetch(config, reset=args.reset, EID=args.EID, SID=args.SID, lazy=args.lazy)


#########
# BUILD #
#########
def build(students, config, lazy=False, skip_latex=False):
    ###################
    # latex/pdf paths #
    ###################
    latex = FileHandler(config["paths"].get("latex", "latex"), is_file=False)
    latex.create_path()
    print(f"LaTex path: {latex}")
    pdf = FileHandler(config["paths"].get("pdf", "pdf"), is_file=False)
    pdf.create_path()
    print(f"PDF path: {pdf}")

    # provisoire
    provisoire = "watermark" in config["latex"]
    watermark = config["latex"].get("watermark")
    print(f"Provisoire: {provisoire}")
    if provisoire:
        print(f"Watermark: {watermark}")

    this_dir = os.path.dirname(os.path.abspath(__file__))
    template = FileHandler([this_dir, "static", "template.tex"])
    logo = FileHandler(config["paths"].get("logo", "logo.png"))
    if logo:
        print(f"Logo: {logo}")
    else:
        print("Le logo n'a pas été trouvé.")
    sign = FileHandler(config["paths"].get("sign", "sign.png"))
    if sign:
        print(f"Signature: {sign}")
    else:
        print("La signature n'a pas été trouvée.")

    # get comments from config
    comments = config.get("comments", {})
    ignored = config.get("scodoc", {}).get("ignored")
    removed = config.get("scodoc", {}).get("removed")
    bc = config.get("scodoc", {}).get("bc")

    ###############################################
    # loop of students to build and compile latex #
    ###############################################
    student_n = len(students)
    for student_i, student_dict in enumerate([v for k, v in students.items()]):

        student = Student(student_dict, comments=comments, ignored=ignored, removed=removed, bc=bc)
        disp = f"{student_i + 1:03d}/{student_n:03d} {student}"
        print(f"{disp:<40}")

        if not len(student.semesters):
            print("error (pas de semestres retenus)")
            continue
        # elif not all(student.followed_semesters):
        #     print(f'error (manque semestres: {", ".join([str(i + 1) for i, f in enumerate(student.followed_semesters) if not f])})')
        #     continue

        # pdf files
        pdf_from = FileHandler(os.path.join(latex.path, student.pdf_from))
        pdf_to = FileHandler(os.path.join(pdf.path, student.pdf_to))

        # skip already compiled files
        if lazy and bool(pdf_to):
            # print(f"ok ({pdf_to})")
            continue

        # variables to define in latex template
        student.init_latex_variables(partition=config.get("scodoc", {}).get("groupe", "Parcours"))
        # student.show_latex_variables()

        # create latex main file from the template (SED)
        replacements = [
            ("SED_VARIABLES", student.define_latex_variables()),
            ("SED_FICHE_NUMBER", student.student_id),
            ("SED_ADDRESS", "\\\\\n    ".join(config["latex"]["address"])),
            ("SED_CITY", config["latex"]["city"]),
            ("SED_NAME", config["latex"]["name"]),
            ("SED_PROVISOIRE", "true" if provisoire else "false"),
            ("SED_WATERMARK", config["latex"].get("watermark", "dummy")),
            ("SED_LOGO", f'{"" if logo else "%"}{chr(92)}includegraphics[height=1cm]{{{logo.file}}}'),
            ("SED_SIGN", f'{"" if sign and not provisoire else "%"}{chr(92)}draw (0.2, 0) node[inner sep=0] {{{chr(92)}includegraphics[width=0.2{chr(92)}textwidth]{{{sign.file}}}}};'),
        ]

        tex = ""
        with open(template.file, "r") as f:
            tex = f.read()
            for a, b in replacements:
                tex = tex.replace(a, b)

        student_tex = FileHandler([latex.path, f"{student.student_id}.tex"])
        with open(student_tex.file, "w") as f:
            f.write(tex)

        if skip_latex:
            return

        # # compile with pdflatex
        command = [f'pdflatex -halt-on-error -output-directory="{latex.path}" "{student_tex.file}" > "{student_tex.file}.log"']
        subprocess.run(command, shell=True, check=True, text=True)
        
        # move pdf
        command = [f'mv "{pdf_from}" "{pdf_to}"']
        subprocess.run(command, shell=True, check=True, text=True)

        # print(f"ok ({pdf_to})")


def build_script():
    """Créé et compile les fiches latex à partir des informations étudiants collectées avec iut-pe-fetch."""

    # args parser
    parser = default_parser("iut-pe-build", build.__doc__)
    parser.add_argument("--lazy", action="store_true", help="Ignore les pdf déjà présents.")
    args = parser.parse_args()

    # load configuration file configuration file
    config = FileHandler(args.config).load(config=True)

    # open students db
    database = FileHandler(config["paths"].get("database", "./etudiants.json"))
    if not database:
        print(f"La base de données {database} n'a pas été trouvée.")
        print("Utiliser en premier lieu la commande iut-pe-fetch afin de construire la base de données.\n")
        raise FileNotFoundError("Database not found")
    print(f"Base de données: {database}")
    etudiants = database.load()

    # run build latex
    build(etudiants, config, lazy=args.lazy)


def send_script():
    """Envoie les fiches latex à partir des informations étudiants collectées avec iut-pe-fetch et les fiches compilées avec iut-pe-build."""
    # args parser
    parser = default_parser("iut-pe-send", build.__doc__)
    args = parser.parse_args()

    # load configuration file configuration file
    config = FileHandler(args.config).load(config=True)

    # open students db
    database = FileHandler(config["paths"].get("database", "./etudiants.json"))
    if not database:
        print(f"La base de données {database} n'a pas été trouvée.")
        print("Utiliser en premier lieu la commande iut-pe-fetch afin de construire la base de données.\n")
        raise FileNotFoundError("Database not found")
    print(f"Base de données: {database}")
    students = database.load()

    pdf = FileHandler(config["paths"].get("pdf", "pdf"), is_file=False)

    try:
        with open(os.path.join(pdf.path, "email_sent.log"), "r") as f:
            sent = [int(_.split(" | ")[0]) for _ in f.readlines()]
    except Exception:
        sent = []

    rate = RateLimiter(frequency=0.2)
    for i, s in enumerate(students.values()):
        disp = f'{i + 1:03d}/{len(students):03d} {s["civilite"]} {s["nom"]} {s["prenom"].title()} [{s["id"]}]'
        print(f"{disp:<40}")

        student = Student(s)

        if int(student.student_id) in sent:
            print("already sent")
            continue

        pdf = FileHandler(os.path.join(pdf.path, student.pdf_to))

        if not pdf:
            print("pdf not found")
            continue

        try:
            student.send(**config["smtp"], pdf=pdf)
        except Exception as e:
            print(f"[ERROR] {e}")
        else:
            print(f"sent to {student.email}")

        rate.sleep()
