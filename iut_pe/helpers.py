#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.

import requests
import os
import urllib3
import math

###########
# CLASSES #
###########


class ScodocAPI:
    """Handles scodoc API connection and requests"""

    def __init__(self, url=None, login=None, password=None, departement=None, **kargs):
        self.url = os.path.join(url, "ScoDoc")
        self.login = login
        self.password = password
        self.token = None
        self.departement = departement

    def __str__(self):
        return f"{self.login}@{self.url}"

    def call(self, path):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        if not self.token:
            # print("get token")
            self.get_token()

        url = os.path.join(self.url, self.departement)
        url = os.path.join(url, "api")
        url = os.path.join(url, path)

        response = requests.get(url, headers={"Authorization": f"Bearer {self.token}"}, verify=False)

        response.raise_for_status()
        return response.json()

    def get_token(self):
        url = os.path.join(self.url, "api", "tokens")
        auth = (self.login, self.password)
        # print(f"[POST] {url} {auth}")
        response = requests.post(url, auth=auth, verify=False)
        response.raise_for_status()
        self.token = response.json()["token"]

    def ping(self):
        print("ping?")
        self.call("etudiants/courants")
        print("pong!")


class FileHandler:
    def __init__(self, path, is_file=True):
        from pathlib import Path
        import os

        if isinstance(path, list):
            path = os.path.join(*path)

        path = Path(path).expanduser().resolve()
        self.is_file = is_file
        if is_file:
            self.file = path
            path, _ = os.path.split(path)
            self.path = Path(path)
        else:
            self.file = None
            self.path = Path(path)

    def __str__(self):
        if self.file:
            return str(self.file)
        else:
            return str(self.path)

    def __bool__(self):
        if self.file:
            return self.file.exists()
        else:
            return self.path.exists()

    def create_path(self):
        if self.path.exists():
            return
        self.path.mkdir(parents=True)

    def delete_file(self):
        if self and self.file:
            print(f"Suppression du fichier {self.file}")
            self.file.unlink()

    def dump(self, data):
        import json

        json.dump(data, open(self.file, "w"), indent=4)

    def load(self, config=False):
        import json
        import yaml

        # load a simple json file
        if not config:
            return json.load(open(self.file, "r"))

        # load a configuration yaml file
        if not self:
            print(f"Le fichier de configuration n'a pas été trouvé à l'emplacement {self}.")
            print("Vérifier le chemin et utiliser l'option --config si besoin.\n")
            raise FileNotFoundError("File not found")

        config = yaml.safe_load(open(self.file, "r"))
        if config.get("paths") is None:
            config["paths"] = {}

        return config


class Student:
    def __init__(self, d: dict, comments: dict = {}, ignored: list = [], removed: list = [], bc: list = []):
        # self.d = d
        self.student_id = str(d["id"])
        self.full_name = f'{d["nom"].upper()} {d["prenom"].title()}'
        self.pdf_from = f"{self.student_id}.pdf"
        self.pdf_to = f'{self.full_name.replace(" ", "_")}_{self.student_id}.pdf'
        self.latex_variables = {}
        self.comments = comments
        self.email = d["adresses"][0]["email"]
        # self.email = "roubin@crans.org"
        self.annotations = d["annotations"]

        semestre_ids = [int(s["semestre_id"]) for s in d["semestres"]]
        self.last_step = max([s["semestre_id"] for s in d["semestres"]]) if len(d["semestres"]) else 0
            
        if not ignored:
            ignored = []
        
        if not removed:
            removed = []
            
        if not bc or type(bc) != list or len(bc) != 5:
            self.bc = [""] * 5
        else:
            self.bc = bc

        # add blank semesters for students that haven't been in scodoc since year 1
        # for i in range(self.last_step):
        #     if not i + 1 in semestre_ids:
        #         d["semestres"].append({
        #             "semestre_id": i + 1,
        #             "resultats": {"ues": dict()}
        #         })
        #         # print(f"[warning] did not follow semester {i + 1}")
        #         removed.append(i + 1)

        # add missing UE in to ues (ie semester 5 might miss some BC)
        for s in d["semestres"]:
            default_ressources = {}

            # test missing with a full semester
            # if int(s["semestre_id"]) == 4:
            #     del s["resultats"]["ues"]["UE4.1"]
            #     del s["resultats"]["ues"]["UE4.3"]
                
            # next loop we're looking for
            # UE1.1, UE1.2, UE1.3, UE1.4, UE1.5
            # 5.1, 5.2, 5.3
            # 2 issues starting at s5:
            #   - no UE in front (not a real problem)
            #   - some UE are missing (usually there's only 3)
            missing_ue = [1, 2, 3, 4, 5]
            root_ue = "dummy"
            for k, v in s["resultats"]["ues"].items():
                # get the root: r = UE1 or U2 or U3 or U4 or 5
                # get the number: n = 1, 2, 3, 4 or 5
                try:
                    root_ue, n = k.split(".")
                except ValueError:
                    # ignore options like Sport
                    continue
                missing_ue.remove(int(n))

                if len(v["ressources"]):
                    default_ressources = v["ressources"]

            # add empty missing UE
            for ue in missing_ue:
                k = f"{root_ue}.{ue}"
                s["resultats"]["ues"][k] = {
                    "moyenne": {"value": -1.0, "moy": -1.0, "rang": 0, "total": 0},
                    "ressources": default_ressources,
                    "missing": True  # adding missing allows to skip partial semesters if a UE a missing on purpose
                }

            # sort ues and filter bonus
            s["resultats"]["ues"] = {
                k: v for k, v in sorted(s["resultats"]["ues"].items()) if "bonus_description" not in v
            }
            
        def keep_semester(s):
            if s["id"] in removed:
                return False
            
            if s["id"] in ignored:

                # manually ignore all marks
                # print(f'Semestre {s["semestre_id"]} neutralisé')
                for k, v in s["resultats"]["ues"].items():
                    default = {"value": "~", "moy": "~", "rang": 0, "total": 0}
                    s["resultats"]["ues"][k]["moyenne"] = default
                    s["resultats"]["ues"][k]["ressources"] = {}

            # if self.get_year() == 2 and s["semestre_id"] == 4:
            #     # but 2 remove s4
            #     return False
            # 
            # if self.get_year() == 3 and s["semestre_id"] > 5:
            #     # but 3 remove s6
            #     return False

            return len(s["resultats"]["ues"]) >= 5


        self.semesters = [Semester(s) for s in d["semestres"] if keep_semester(s)]

        # recompute last step in case one semester has been filtered
        self.last_step = max([s.semester_step for s in self.semesters]) if len(self.semesters) else 0        
        # check if studend has followed all semesters
        self.followed_semesters = [i + 1 in [s.semester_step for s in self.semesters] for i in range(6)]


    def __str__(self):
        return f"{self.full_name} [{self.student_id}]"

    def get_year(self):
        """ returns the year number (BUT1, 2 or 3)
        """
        if self.last_step <= 2:
            return 1

        # if self.last_step < 4:
        if self.last_step <= 4:
            return 2

        return 3
        
    def get_but(self):
        return self.semesters[0].title

    def get_promotion(self):
        return self.semesters[0].start.split("/")[-1]

    def get_parcours(self, partition=None):
        from unidecode import unidecode

        cursus = []
        for semester in self.semesters:
            if len(semester.cursus) == 1:
                #  get cursus from semester cursus if only one available
                c = semester.cursus[0]["name"]
            else:
                # get parcours from group
                groups_with_partiton = [g for g in semester.groups if g["partition"] == partition]
                if not len(groups_with_partiton):
                    # can't find partition
                    continue
                c = groups_with_partiton[0]["name"]

            if c not in cursus:
                cursus.append(c)

            # print(f"[debug] get cursus {semester}: {', '.join([unidecode(c) for c in cursus])} ({partition})")

        # return ", ".join([unidecode(c) for c in cursus])
        if len(cursus):
            return unidecode(cursus[-1])

        return "N/A"

    def get_semesters(self):
        return ", ".join([s.name for s in self.semesters])

    def get_title(self):
        y = self.get_year()
        if y == 1:
            return "réorientation BUT 1"
        elif y == 2:
            return "réorientation BUT 2"
        elif y == 3:
            return "poursuite d'études BUT 3"
        else:
            raise ValueError("Pas d'année assignée")

    def get_competences_dict(self):
        for semester in self.semesters:
            for competences in [_["competences"] for _ in semester.cursus if len(_["competences"]) == 5]:
                # for the first sets of 5 competences in the first semester returns the dict with latex keys
                return {"bc" + "abcde"[i]: bc for i, bc in enumerate(competences)}

        print("\t[warning] Les 5 blocs de compétances n'ont pas été trouvés")
        return {"bc" + "abcde"[i]: bc for i, bc in enumerate(self.bc)}

    def get_results_dict(self):
        results = {}

        # apo comments
        apo = self.comments.get("apo", {})

        comments = []

        # print()

        # ADM 	VAL 	Acquis
        # ADJ 	CODJ 	Acquis par décision du jury
        # ADSUP 	Acquis parce que le niveau de compétence supérieur est acquis
        # CMP 		Acquis par compensation annuelle

        # RAT 		En attente d'un rattrapage
        # AJ 	AJ 	Attente pour problème de moyenne

        # ATJ 	nd 	Non validé pour une autre raison, voir règlement local
        
        # DEF 		Défaillant
        # ABAN 		Non évalué pour manque assiduité

        JURY_TO_COMP = {
            "ADM": "Acquis \cmark",
            "ADJ": "Acquis \cmark",
            "ADSUP": "Acquis \cmark",
            "CMP": "Acquis \cmark",
            "RAT": "En cours \pmark",
            "AJ": "En cours \pmark",
            None: "En cours \pmark",  # jury pas passé
            "ATJ": "???",
            "ABAN": "Non Acquis \\xmark",
            "DEF": "Non Acquis \\xmark",
            "N/A": "\greycell",  # comp pas évaluée
        }

        comp_valid = {}
        
        for semester in self.semesters:
            # print(f"[debug] {semester} [{semester.latex_key}]")
            results[semester.latex_key] = semester.name
            year = math.ceil(semester.semester_step / 2)
            # print(f"Level {year}")
            

            if len(semester.competences) != 5 and year in comp_valid and comp_valid[year] != semester.competences:
                print(f"\t[warning] overwrite competence (capitalisation)")
                semester.competences = comp_valid[year].copy()
            else:
                comp_valid[year] = semester.competences.copy()
                
            # loop over the UE
            # if semester.semester_step % 2:
            #     print(f"[debug] {semester} [{semester.latex_key}]")
            for ue_i, ue in enumerate(semester.ues):
                all_comp = len(comp_valid[year]) == 5

                # if ue_i > 4:
                #     # sport, etc...
                #     print(f'[warning] skipping ue {ue_i}: {ue["titre"]}')
                #     continue

                ue_marks = ue["moyenne"]
                latex_key = semester.latex_key + "c" + "abcde"[ue_i]
                latex_key_comp = "comp" + "abcde"[ue_i] + " abc"[year]
                
                # get ue marks
                if ue_marks["value"] == "~":
                    results[latex_key + "a"] = "\pmark"
                    results[latex_key + "b"] = "\pmark"
                    results[latex_key + "c"] = "\pmark"
                    jury = semester.competences.pop(0)
                elif float(ue_marks["value"]) < 0:
                    results[latex_key + "a"] = "\greycell"
                    results[latex_key + "b"] = "\greycell"
                    results[latex_key + "c"] = "\greycell"
                    jury = semester.competences.pop(0) if all_comp else "N/A"  # compensation if all_comp else bc not in formation
                else:
                    results[latex_key + "a"] = float(ue_marks["value"])
                    results[latex_key + "b"] = float(ue_marks["moy"])
                    rang = int(str(ue_marks["rang"]).split(" ")[0])  # handle "13 ex"
                    results[latex_key + "c"] = f'{rang:03d}/{int(ue_marks["total"]):03d}'
                    jury = semester.competences.pop(0)

                results[latex_key_comp] = JURY_TO_COMP[jury]
                if jury in ["ATJ", "ABAN", "DEF"]:
                    print(f"\t[notice] competence non valide: {jury}")

                # if semester.semester_step % 2:
                # print(f"\t BC {ue_i + 1} -> {jury}")
                
                # get ressources mark (overwrite for each eu but it should be the same)
                ue_ressources = ue["ressources"]

                # potentially modify the keys from R5.01 to MAT5
                #                              and MAT1 (coeff..) to MAT1
                old_keys = [f"R{semester.semester_step}.01", f"R{semester.semester_step}.02", f"R{semester.semester_step}.03", "MAT", "COM", "ANG"]
                new_keys = [f"MAT{semester.semester_step}", f"COM{semester.semester_step}", f"ANG{semester.semester_step}"] * 2
                for o, n in zip(old_keys, new_keys):
                    for k in [k for k in ue_ressources if o in k]:
                        ue_ressources[n] = ue_ressources[k]

                r_keys = [r + f"{semester.semester_step}" for r in ["MAT", "COM", "ANG"]]
                for r_i, r_key in enumerate(r_keys):
                    latex_key = semester.latex_key + "r" + "abc"[r_i]                        
                    mark = ue_ressources.get(r_key, {"moyenne": -1})["moyenne"]
                    if semester.semester_step == 6:
                        mark = "\greycell"
                    elif mark  == "~":
                        mark = "\pmark"
                    elif float(mark) < 0:
                        mark = "\greycell"
                        
                    results[latex_key] = mark
                    # print(latex_key)
                    
            # global rank
            rang = int(str(semester.rank["value"]).split(" ")[0])  # handle "13 ex"
            total = int(semester.rank["total"])
            results[semester.latex_key + "rank"] = f"{rang:03d}/{total:03d}"

            if semester.in_progress:
                results[semester.latex_key] += "$^\dagger$"  # noqa: W605
                results["partialsemester"] = True
                results[semester.latex_key + "rank"] = "\pmark"

            # apo comments
            for k, v in apo.items():
                if k in semester.codes:
                    comments.append(f"{semester.name}: {v}")

        if "all" in self.comments:
            comments.append(self.comments["all"])

        if len(comments):
            results["comments"] = "".join([f"\\item {c}." for c in comments])

        return results

    def get_assiduity(self):
        abs_inj = 0
        abs_tot = 0
        abs_met = ""
        for semester in self.semesters:
            abs_inj += semester.assiduity["injustifie"]
            abs_tot += semester.assiduity["total"]
            abs_met = semester.assiduity["metrique"].split()[0]

        def _s(n):
            return "s" if n > 1 else ""

        return f"{abs_inj} absence{_s(abs_inj)} injustifiée{_s(abs_inj)} pour {abs_tot} absence{_s(abs_tot)} au total sur {len(self.semesters)} semestres ({abs_met} journée)."

    def get_alternant(self):
        # get ALT
        
        # fi = [s.modality.lower() in ["formation initiale", "fi"] for s in self.semesters]
        # return "Formation Initiale" if all(fi) else "Alternant"

        # get from annotations
        alt = bool(len([_['comment'] for _ in self.annotations if 'formation:alt' in _['comment'].replace(' ', '').lower()]))
        formation = "Alternance" if alt else "Formation Initiale"
        print(f"\t[notice] Formation: {formation}")
        return formation

    def get_avis(self):
        if not self.get_year() == 3:
            return False  #"le département ne fournit d'avis que pour les étudiants de BUT 3"

        # get from formula
        n = 0
        p = 0
        for step, ue in [(s.semester_step, ue["moyenne"]) for s in self.semesters for ue in s.ues if ue["moyenne"]["total"]]:
            coef = step
            p += coef * float(str(ue["rang"]).split()[0]) / float(ue["total"])
            n += coef

        percentil = 100 * p / float(n)
        if p / float(n) < 0.15:
            avis = f"Très favorable"

        elif p / float(n) < 0.5:
            avis = f"Favorable"

        elif p / float(n) < 0.85:
            avis = f"Neutre"

        else:
            avis = f"Réservé"
        
        try:
            # get from annotations
            tmp = [_['comment'].split(':')[1].strip() for _ in self.annotations if 'pe:' in _['comment'].replace(' ', '').lower()][0]
            tmp = tmp[0].upper() + tmp[1:]
            print(f"\t[notice] {avis} -> {tmp}")
            # avis += f" modifié en {tmp}"
            avis = tmp
        except IndexError:
            pass

        # avis += f" (top {percentil:.2f}\\%)"
        # print(avis)
        return avis

    def init_latex_variables(self, partition=None, semester_treshold=None):

        self.latex_variables = {
            "titre": self.get_title(),
            "parcours": self.get_parcours(partition=partition),
            "alternant": self.get_alternant(),
            "candidat": self.full_name,
            "but": self.get_but(),
            "promotion": self.get_promotion(),
            "semestres": self.get_semesters(),
            "assiduite": self.get_assiduity()
        }

        for k, v in self.get_competences_dict().items():
            self.latex_variables[k] = v

        for k, v in self.get_results_dict().items():
            self.latex_variables[k] = v

        avis = self.get_avis()
        if avis:
            self.latex_variables["avis"] = avis

        if self.get_year() == 3:
            self.latex_variables["buttrois"] = True

    def show_latex_variables(self):
        for k, v in self.latex_variables.items():
            print(f"Latex variable {k:<10}: {v}")
        # print(self.define_latex_variables())

    def define_latex_variables(self):
        return "".join([f"{chr(92)}def{chr(92)}{k}{{{v}}}\n" for k, v in self.latex_variables.items()])

    def send(self, login=None, password=None, email=None, url=None, pdf=""):
        import smtplib
        from email.mime.multipart import MIMEMultipart
        from email.mime.base import MIMEBase
        from email.mime.text import MIMEText
        from email import encoders

        if self.get_year() == 3:
            subject = "Fiche de poursuite d'études"
            body = "Veuillez trouver ci-joint votre fiche de poursuite d'études."
        else:
            subject = "Fiche de réorientation"
            body = "Veuillez trouver ci-joint votre fiche de réorientation."

        # Setup the MIME
        message = MIMEMultipart()
        message["From"] = email
        message["To"] = self.email
        message["Subject"] = subject

        # Add body to email
        message.attach(MIMEText(body, "plain"))

        # Open the file in binary mode
        with open(pdf.file, "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "pdf")
            part.set_payload(attachment.read())
            pdf_name = str(pdf.file).split("/")[-1]
            part.add_header("Content-Disposition", f'attachment; filename="{pdf_name}"')

        # Encode file in ASCII characters to send by email
        encoders.encode_base64(part)

        # Add attachment to message and convert message to string
        message.attach(part)
        text = message.as_string()

        # Create secure connection with server and send email
        try:
            with smtplib.SMTP(url, 587) as server:
                server.starttls()  # Start TLS encryption
                server.login(login, password)
                server.sendmail(email, self.email, text)
            with open(os.path.join(pdf.path, "email_sent.log"), "a") as f:
                f.write(f"{self.student_id} | {self.full_name} | {email} | {self.email}\n")
        except Exception as e:
            with open(os.path.join(pdf.path, "email_error.log"), "a") as f:
                f.write(f"{self.student_id} | {self.full_name} | {email} | {self.email} | {e}\n")


class Semester:
    def __init__(self, d: dict):
        # for k, v in d.items():
        #     print(f"[debug] semester {k:<10}: {v}")

        self.semester_id = d["id"]
        self.semester_step = d["semestre_id"]
        self.i = d["i"]
        self.title = d["formation"]["titre_officiel"].replace("BUT", "").strip()
        self.start = d["date_debut"]
        self.end = d["date_fin"]
        self.year = d["annee_scolaire"]
        self.modality = d["modalite"]
        self.rank = d["rang"]
        self.in_progress = bool(d["etat"])
        self.name = f"Semestre {self.semester_step} {self.year}/{self.year + 1}"
        
        # latex keys
        # self.latex_key = "sem" + "abcdef"[self.semester_step - 1]
        self.latex_key = "sem" + "abcdefgh"[self.i]

        # dicts
        self.assiduity = d["absences"]

        # list of dict
        self.ues = [ue for i, ue in enumerate(d["resultats"]["ues"].values()) if i < 5]
        self.ues_cap = [ue for i, ue in enumerate(d["resultats"]["ues_capitalisees"].values()) if i < 5]
        
        self.cursus = [{"code": p["code"], "name": p["libelle"], "competences": [k for k in p["annees"][str((self.semester_step + 1) // 2)]["competences"].keys()]} for p in d["parcours"]]
        self.groups = [{"name": g["group_name"], "partition": g["partition"]["partition_name"]} for g in d["groupes"]]
        self.competences = [_["code"] for _ in d["competences"]] if len(d["competences"]) else [None] * 5
        
        # code
        if d["elt_annee_apo"]:
            self.codes = d["elt_annee_apo"].split(",")
        else:
            self.codes = []

    def __str__(self):
        return f'S{self.semester_step} [{self.semester_id}] {self.title} {self.start} -> {self.end} ({", ".join(self.codes)}){" En cours" if self.in_progress else ""}'


def is_float(n):
    try:
        float(n)
        return True
    except Exception:
        return False


def default_parser(prog, doc):
    import argparse

    parser = argparse.ArgumentParser(prog=prog, description=doc, epilog="---")
    parser.add_argument(
        "--config",
        default="config.yml",
        type=str,
        help="Chemin vers le fichier de configuration (défaut: répertoire courant).",
    )
    return parser


def handle_accents(s):
    import unicodedata

    # for now it just remove the accents :(
    return "".join((c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"))
